package com.example.server_spring_lb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
public class ServerSpringLbApplication implements CommandLineRunner {

    @Autowired
    RestTemplate restLBTemplate;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    LoadBalancerClient loadBalancerClient;

    public static void main(String[] args) {
        SpringApplication.run(ServerSpringLbApplication.class, args);

//        ConfigurableApplicationContext ctx = new SpringApplicationBuilder(ServerSpringLbApplication.class)
//                .web(WebApplicationType.NONE)
//                .run(args);
//
//        WebClient loadBalancedClient = ctx.getBean(WebClient.Builder.class).build();
//
//        for (int i = 1; i <= 10; i++) {
//            String response =
//                    loadBalancedClient.get().uri("http://example-service/api/print")
//                            .retrieve().toEntity(String.class)
//                            .block().getBody();
//            System.out.println(response);
//        }
    }

    @Override
    public void run(String... args) throws Exception {


//        for (int i = 1; i <= 10; i++) {
//            String url = "http://example-service/api/print";
//            String response = restLBTemplate.getForObject(url, String.class);
//            System.out.println(response);
//        }

        for(int i = 0; i <= 10; i++){
            var res = loadBalancerClient.choose("example-service");
            String url = null;
            switch (res.getInstanceId()) {
                case "example-service1":
                    url = res.getUri().resolve("/api/print02").toString();
                    break;
                case "example-service2":
                    url = res.getUri().resolve("/api/print").toString();
                    break;
            }

            String response = restTemplate.getForObject(url, String.class);
            System.out.println(response);

        }

    }
}
