package com.example.instagrambackend.exception;

import com.example.instagrambackend.util.ErrorCodeConst;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class InstagramException extends RuntimeException {
    private ErrorCodeConst errorCodeConst;
    private String message;
}
