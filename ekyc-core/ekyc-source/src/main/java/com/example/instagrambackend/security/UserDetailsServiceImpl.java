package com.example.instagrambackend.security;

import com.example.instagrambackend.exception.InstagramException;
import com.example.instagrambackend.util.ErrorCodeConst;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.function.Function;

@Slf4j
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserPasswordDefine userPasswordDefine;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Map<String, String> userPasswordMapper = userPasswordDefine.getUserPasswordMapper();
        if(!userPasswordMapper.containsKey(username)){
            log.error("username không đúng, username: {}", username);
            throw new InstagramException(ErrorCodeConst.UNAUTHORIZED, null);
        }

        PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        Function<String, String> encoder = passwordEncoder::encode;
        String password = encoder.apply(userPasswordMapper.get(username));

        return new UserDetailsInsta(username, password);
    }
}
