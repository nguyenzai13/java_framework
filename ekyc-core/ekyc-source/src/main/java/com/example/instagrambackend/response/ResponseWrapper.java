package com.example.instagrambackend.response;

/* Description: Define a wrapper of correct response */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ResponseWrapper<T> {

    private boolean success;
    private String errorCode;
    private String traceId;
    private String message;
    private T data;

    public static <T> ResponseWrapper createResponseWrapper(T data) {
        ResponseWrapper<T> responseWrapper = new ResponseWrapper<>();
        responseWrapper.setData(data);
        return responseWrapper;
    }

    public static <T> Object paginated(PaginationMetadata paginationMetadata, T items){
        Map<String, Object> dataRes = new HashMap<>();
        dataRes.put("page_data", items);
        dataRes.put("pagination", paginationMetadata);
        return dataRes;
    }

    @Getter
    @Accessors(chain = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    @AllArgsConstructor
    public static class PaginationMetadata {
        private final int limit;
        private final long totalElement;
        private final int totalPages;
        private final int currentPage;
    }

}
