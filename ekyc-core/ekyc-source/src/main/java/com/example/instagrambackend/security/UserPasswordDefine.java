package com.example.instagrambackend.security;

import lombok.Data;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
@Data
public class UserPasswordDefine {

    private Map<String, String> userPasswordMapper = new HashMap<>();

    public UserPasswordDefine(){
        userPasswordMapper.put("dainq","daideptrai");
        userPasswordMapper.put("minh","minhxinhgai");
    }

}
