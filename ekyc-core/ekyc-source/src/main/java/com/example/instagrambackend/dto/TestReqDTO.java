package com.example.instagrambackend.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TestReqDTO {
    private String data;
}
