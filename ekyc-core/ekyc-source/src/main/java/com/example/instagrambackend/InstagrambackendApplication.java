package com.example.instagrambackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InstagrambackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(InstagrambackendApplication.class, args);
    }

}
