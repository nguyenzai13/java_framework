package com.example.instagrambackend.exception;

import com.example.instagrambackend.response.ResponseFactory;
import com.example.instagrambackend.response.ResponseWrapper;
import com.example.instagrambackend.util.ErrorCodeConst;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class InstagramExceptionHandler {

    private final ResponseFactory responseFactory;

    @ExceptionHandler(Exception.class)
    public@ResponseBody
    ResponseEntity<ResponseWrapper<Object>> handleAllException(Exception e, HttpServletRequest request, HttpServletResponse response){
        return responseFactory.fail(null, ErrorCodeConst.INTERNAL_SERVER_ERROR, null);
    }

    @ExceptionHandler(InstagramException.class)
    public @ResponseBody
    ResponseEntity<ResponseWrapper<Object>> handleInstagramException(Exception e, HttpServletRequest request, HttpServletResponse response){
        InstagramException instagramException = (InstagramException) e;
        return responseFactory.fail(null, instagramException.getErrorCodeConst(), instagramException.getMessage());
    }

}
