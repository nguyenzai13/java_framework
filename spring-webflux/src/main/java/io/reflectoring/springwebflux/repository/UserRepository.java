package io.reflectoring.springwebflux.repository;

import io.reflectoring.springwebflux.entity.UserEntity;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends R2dbcRepository<UserEntity, Integer> {
    Mono<UserEntity> findFirstByAge(Integer age);
    Mono<UserEntity> findById(Integer age);
}
