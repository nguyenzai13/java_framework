package io.reflectoring.springwebflux.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Table("user")
public class UserEntity {

    @Id
    private Integer id;

    @Column("name")
    private String name;

    @Column("age")
    private Integer age;

    @Column("salary")
    private Double salary;

    @Column("department")
    private String department;
}
