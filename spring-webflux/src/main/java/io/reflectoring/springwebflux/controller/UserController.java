package io.reflectoring.springwebflux.controller;

import io.reflectoring.springwebflux.entity.UserEntity;
import io.reflectoring.springwebflux.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<UserEntity> create(@RequestBody UserEntity user){
        return userService.createUser(user);
    }

    @GetMapping
    public Flux<UserEntity> getAllUsers(){
        return userService.getAllUsers();
    }

    @GetMapping("/{userId}")
    public Mono<ResponseEntity<UserEntity>> getUserById(@PathVariable Integer userId){
        Mono<UserEntity> user = userService.findById(userId);
        return user.map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PutMapping("/{userId}")
    public Mono<ResponseEntity<UserEntity>> updateUserById(@PathVariable Integer userId, @RequestBody UserEntity user){
        return userService.updateUser(userId,user)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.badRequest().build());
    }

    @DeleteMapping("/{userId}")
    public Mono<ResponseEntity<Void>> deleteUserById(@PathVariable Integer userId){
        return userService.deleteUser(userId)
                .map( r -> ResponseEntity.ok().<Void>build())
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

//    @GetMapping("/search")
//    public Flux<UserEntity> searchUsers(@RequestParam("name") String name) {
//        return userService.fetchUsers(name);
//    }
}
