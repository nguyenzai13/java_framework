package io.reflectoring.springwebflux.service;

import io.reflectoring.springwebflux.entity.UserEntity;
import io.reflectoring.springwebflux.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Slf4j
@RequiredArgsConstructor
@Transactional
public class UserService {

    private final UserRepository userRepository;

    public Mono<UserEntity> createUser(UserEntity user){
        return userRepository.save(user);
    }

    public Flux<UserEntity> getAllUsers(){
        return userRepository.findAll();
    }

    public Mono<UserEntity> findById(Integer userId){
        return userRepository.findById(userId);
    }

    public Mono<UserEntity> updateUser(Integer userId,  UserEntity user){
        return userRepository.findById(userId)
                .flatMap(dbUser -> {
                    dbUser.setAge(user.getAge());
                    dbUser.setSalary(user.getSalary());
                    return userRepository.save(dbUser);
                });
    }

    public Mono<UserEntity> deleteUser(Integer userId){
        return userRepository.findById(userId)
                .flatMap(existingUser -> userRepository.delete(existingUser)
                        .then(Mono.just(existingUser)));
    }

//    public Flux<User> fetchUsers(String name) {
//        Query query = new Query()
//                .with(Sort
//                        .by(Collections.singletonList(Sort.Order.asc("age")))
//                );
//        query.addCriteria(Criteria
//                .where("name")
//                .regex(name)
//        );
//
//        return reactiveMongoTemplate
//                .find(query, User.class);
//    }
}