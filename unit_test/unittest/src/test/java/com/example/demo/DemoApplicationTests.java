package com.example.demo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

//@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class DemoApplicationTests {

    @Mock
    private List<String> mockedList;

    @Test
    public void contextLoads() {
        // Trả là size 100 khi gọi hàm size()
        Mockito.when(mockedList.size()).thenReturn(2);
        Assert.assertEquals(2, mockedList.size());
    }

}
