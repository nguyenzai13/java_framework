package com.example.server.config;

import com.netflix.loadbalancer.IPing;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.PingUrl;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;

public class RibbonConfig {

    @Bean
    public IPing ribbonPing() {
        return new PingUrl(false, "/api/ping");
    }

    @Bean
    public IRule ribbonRule() {
        return new RoundRobinRule();
    }
}
