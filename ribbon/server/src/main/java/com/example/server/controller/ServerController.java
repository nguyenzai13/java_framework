package com.example.server.controller;

import com.example.server.config.RibbonConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api")
@RibbonClient(
        name = "print-service",
        configuration = RibbonConfig.class)
public class ServerController {

    @Autowired
    RestTemplate printRestTemplate;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    LoadBalancerClient loadBalancerClient;

    @GetMapping("/calling-print")
    public String print() {
        String url = "http://print-service/api/print";
        return printRestTemplate.getForObject(url, String.class);
    }

    @GetMapping("/calling-print-2")
    public String print2() {
        var res = loadBalancerClient.choose("print-service");
        String url = null;
        switch (res.getInstanceId()) {
            case "localhost:8085":
                url = res.getUri().resolve("/api/print02").toString();
                break;
            case "localhost:8086":
                url = res.getUri().resolve("/api/print").toString();
                break;
        }
        return restTemplate.getForObject(url, String.class);
    }
}
