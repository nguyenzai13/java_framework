package com.example.demo;

import static org.awaitility.Awaitility.await;
import org.junit.jupiter.api.Test;
import java.util.List;
import java.util.concurrent.SubmissionPublisher;
import java.util.concurrent.TimeUnit;
import static org.assertj.core.api.Assertions.assertThat;

public class EndSubscriberTest {

    @Test
    public void givenPublisher_whenSubscribeToIt_thenShouldConsumeAllElements() throws InterruptedException {
        //given
        SubmissionPublisher<String> publisher = new SubmissionPublisher<>();
        EndSubscriber<String> subscriber = new EndSubscriber<>();
        publisher.subscribe(subscriber);
        List<String> items = List.of("1", "x", "2", "x", "3", "x");
        //when
        assertThat(publisher.getNumberOfSubscribers()).isEqualTo(1);
        items.forEach(publisher::submit);
        publisher.close();

        //then

        await().atMost(1000, TimeUnit.MILLISECONDS).untilAsserted(
                () -> assertThat(subscriber.consumedElements).containsExactlyElementsOf(items)
        );


    }
}
